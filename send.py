import codecs
import csv
import logging
import os
import smtplib
import sys
import traceback
from time import sleep
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from config import FROM, RECIPIENTS, SUBJECT, TXT_TEMPLATE, LOGIN, PASSWORD, HOST, PORT, SSL, TLS, HTML_TEMPLATE, ENCODING, REPLY_TO

logging.basicConfig(format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
logging.getLogger().setLevel(logging.INFO)

logger = logging.getLogger(__name__)


def remove_bom(line) -> str:
    if type(line) == str:
        return line[3:] if line.encode('utf8').startswith(codecs.BOM_UTF8) else line
    else:
        return line[3:] if line.startswith(codecs.BOM_UTF8) else line


with open(TXT_TEMPLATE, 'r') as file:
    text_mail_template = file.read()

html_mail_template = ''

if os.path.isfile(HTML_TEMPLATE):
    with open(HTML_TEMPLATE, 'r') as file:
        html_mail_template = file.read()
else:
    logging.info("HTML version of the mail is not available")

with open(RECIPIENTS) as csv_file:
    reader = csv.reader((remove_bom(line) for line in csv_file), delimiter=",")
    headers = next(reader, None)

    for row in reader:
        try:
            text_content = text_mail_template
            html_content = html_mail_template
            recipient = None
            for header, value in zip(headers, row):
                if header == "EMAIL":
                    recipient = value
                text_content = text_content.replace("%%" + header + "%%", value)
                html_content = html_content.replace("%%" + header + "%%", value)
            if recipient is None or recipient == "":
                logger.warning("No email provided: " + ';'.join(row))
                continue

            message = MIMEMultipart("alternative")
            message["Subject"] = SUBJECT
            message["From"] = FROM
            message["To"] = recipient
            message['reply-to'] = REPLY_TO


            message.attach(MIMEText(text_content, "plain", ENCODING))

            if html_content != "":
                # Add HTML/plain-text parts to MIMEMultipart message
                # The email client will try to render the last part first
                message.attach(MIMEText(html_content, "html", ENCODING))

            if SSL:
                logger.warning("SSL support is not yet implemented")
                # # Create secure connection with server and send email
                # context = ssl.create_default_context()
                # with smtplib.SMTP_SSL(HOST, PORT, context=context) as server:
                #     server.login(LOGIN, PASSWORD)
                #     server.sendmail(
                #         FROM, recipient, message.as_string()
                #     )
                continue
            with smtplib.SMTP(HOST, PORT) as server:
                if TLS:
                    server.starttls()  # Secure the connection
                if PASSWORD!='':
                    server.login(LOGIN, PASSWORD)
                server.sendmail(
                    FROM, [recipient, FROM], message.as_string()
                )
                logger.info("Mail sent successfully to: " + recipient)
                sleep(0.1)

        except:
            traceback.print_exc(file=sys.stdout)
            logger.warning("Problematic data: " + ';'.join(row))
